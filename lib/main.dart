import 'package:flutter/material.dart';
import 'package:walker/views/home.dart';
import 'package:walker/views/path_creation.dart';
import 'package:walker/views/path_location.dart';
import 'package:walker/views/path_search.dart';
import 'package:walker/views/path_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Walker',
        theme: ThemeData(
            primarySwatch: Colors.green,
            primaryColor: Color(0xFF1A936F),
            accentColor: Color(0xFF114B5F),
            buttonColor: Color(0xFF114B5F),
            floatingActionButtonTheme: FloatingActionButtonThemeData(
                backgroundColor: Color(0xFF1A936F)),
            textTheme: TextTheme(
                bodyText1: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.w400,
                    fontSize: 14),
                headline1: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
                subtitle1: TextStyle(color: Colors.black45, fontSize: 14))),
        initialRoute: '/',
        routes: {
          '/': (context) => Home(),
          '/create': (context) => PathCreation(),
          '/create/location': (context) => PathLocation(),
          '/view': (context) => PathView(),
          '/search': (context) => PathSearch()
        });
  }
}
