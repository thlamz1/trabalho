// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pathProvider.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PathProvider on PathBase, Store {
  final _$currentPathAtom = Atom(name: 'PathBase.currentPath');

  @override
  MapPath get currentPath {
    _$currentPathAtom.reportRead();
    return super.currentPath;
  }

  @override
  set currentPath(MapPath value) {
    _$currentPathAtom.reportWrite(value, super.currentPath, () {
      super.currentPath = value;
    });
  }

  final _$PathBaseActionController = ActionController(name: 'PathBase');

  @override
  void add(MapPath path) {
    final _$actionInfo =
        _$PathBaseActionController.startAction(name: 'PathBase.add');
    try {
      return super.add(path);
    } finally {
      _$PathBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic remove(MapPath path) {
    final _$actionInfo =
        _$PathBaseActionController.startAction(name: 'PathBase.remove');
    try {
      return super.remove(path);
    } finally {
      _$PathBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCurrent(int index) {
    final _$actionInfo =
        _$PathBaseActionController.startAction(name: 'PathBase.setCurrent');
    try {
      return super.setCurrent(index);
    } finally {
      _$PathBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currentPath: ${currentPath}
    ''';
  }
}
