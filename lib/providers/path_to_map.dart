import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:walker/models/path.dart';
import 'package:walker/views/path_info.dart';

String getRandString(int len) {
  var random = Random.secure();
  var values = List<int>.generate(len, (i) => random.nextInt(255));
  return base64UrlEncode(values);
}

Set<Marker> pathToMarkers(MapPath path, BuildContext context,
    {Function onTap}) {
  var index = -1;
  return path.waypoints.map((element) {
    index++;
    return Marker(
        markerId: MarkerId(getRandString(12)),
        position: element.pathTo.last,
        onTap: () => onTap(element, context));
  }).toSet();
}

Set<Polyline> pathToPolylines(MapPath path, BuildContext context,
    {Function onTap}) {
  var index = -1;
  return path.waypoints.map((element) {
    index++;
    return Polyline(
        polylineId: PolylineId(getRandString(12)),
        color: Colors.blueAccent,
        width: 3,
        points: element.pathTo,
        onTap: () => onTap(element, context));
  }).toSet();
}
