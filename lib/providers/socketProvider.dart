import 'package:phoenix_wings/phoenix_wings.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/pathProvider.dart';

class SocketProvider {
  static void connect(PathProvider provider) {
    var socket = new PhoenixSocket(
        'wss://lit-depths-09955.herokuapp.com/socket/websocket');
    socket.connect();
    var trackChannel = socket.channel("tracks:list");

    trackChannel.on("newTrack", (payload, ref, joinRef) {
      MapPath path = MapPath.fromMap(payload["data"]);
      provider.add(path);
    });

    trackChannel.on("updateTrack", (payload, ref, joinRef) {
      MapPath path = MapPath.fromMap(payload["data"]);
      provider.remove(provider.paths.where((p) => p.id == path.id).first);
      provider.add(path);
    });

    trackChannel.on("removeTrack", (payload, ref, joinRef) {
      int id = int.parse(payload["data"]);
      provider.remove(provider.paths.where((p) => p.id == id).first);
    });

    trackChannel.join().receive("ok", (response) {
      print("Joined!");
      for (var path in response["data"]) {
        MapPath mapPath = MapPath.fromMap(path);
        provider.add(mapPath);
      }
    });
  }
}
