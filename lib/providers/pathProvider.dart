import 'package:mobx/mobx.dart';
import 'package:walker/models/path.dart';
part 'pathProvider.g.dart';

PathBase instance;

class PathProvider extends PathBase with _$PathProvider {
  static getInstance() {
    if (instance == null) {
      instance = PathProvider();
    }
    return instance;
  }
}

abstract class PathBase with Store {
  final paths = ObservableList<MapPath>.of([]);

  @observable
  MapPath currentPath = MapPath();

  @action
  void add(MapPath path) {
    paths.add(path);
  }

  @action
  remove(MapPath path) {
    paths.remove(path);
  }

  @action
  void setCurrent(int index) {
    currentPath = paths[index];
  }
}
