import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:walker/models/category.dart';
import 'package:walker/models/category_type.dart';

// Widget ee exibição de categoria
class CategoryPill extends StatelessWidget {
  final Category category;

  const CategoryPill(this.category);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
              margin: EdgeInsets.only(right: 2),
              child: Icon(category.icon,
                  color: categoryToTextColor(category.type), size: 20)),
          Text(
            category.name,
            style: TextStyle(color: categoryToTextColor(category.type)),
          )
        ],
        mainAxisSize: MainAxisSize.min,
      ),
      padding: EdgeInsets.symmetric(vertical: 3, horizontal: 8),
      margin: EdgeInsets.symmetric(horizontal: 4),
      decoration: BoxDecoration(
          color: categoryToBackgroundColor(category.type),
          borderRadius: BorderRadiusDirectional.all(Radius.elliptical(10, 10))),
    );
  }
}
