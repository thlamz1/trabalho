import 'dart:ui';

import 'package:flutter/material.dart';

// Tipo de categoria
enum CategoryType { info, warning, danger, detail, enviroment }

// Cor de background da categoria
Color categoryToBackgroundColor(category) {
  switch (category) {
    case CategoryType.info:
      return Colors.blueAccent;
    case CategoryType.warning:
      return Colors.yellowAccent[400];
    case CategoryType.danger:
      return Colors.red[600];
    case CategoryType.detail:
      return Colors.black12;
    case CategoryType.enviroment:
      return Colors.green[400];
    default:
      return Colors.white;
  }
}

// Cor de texto da categoria
Color categoryToTextColor(category) {
  switch (category) {
    case CategoryType.warning:
      return Colors.black;
    case CategoryType.danger:
      return Colors.black;
    case CategoryType.detail:
      return Colors.black;
    default:
      return Colors.white;
  }
}
