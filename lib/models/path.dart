import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:image/image.dart' as image;
import 'package:walker/models/category.dart';
import 'package:walker/models/waypoint.dart';

class MapPath {
  final int id;
  ImageProvider thumbnail;
  List<Waypoint> waypoints = [];
  String titulo;
  String descricao;
  List<Category> categorias = [];

  final Function onSelect;

  MapPath(
      {waypoints,
      this.id,
      this.titulo = "",
      this.descricao = "",
      categorias,
      this.onSelect,
      this.thumbnail}) {
    this.waypoints = waypoints ?? [];
    this.categorias = categorias ?? [];
  }

  addWaypoint(Waypoint waypoint) {
    waypoints.add(waypoint);
  }

  removeWaypoint(Waypoint waypoint) {
    print(waypoints);
    var waypointIndex = waypoints.indexOf(waypoint);

    if (waypointIndex < waypoints.length - 1 && waypointIndex > 0) {
      waypoints[waypointIndex + 1].pathTo = [
        ...waypoints[waypointIndex].pathTo,
        ...waypoints[waypointIndex + 1].pathTo
      ];
    } else if (waypointIndex < waypoints.length - 1) {
      waypoints[1].pathTo = [waypoints[1].pathTo.last];
    }
    waypoints.removeAt(waypointIndex);
  }

  static MapPath fromMap(Map<String, dynamic> jsonMap, {onSelect}) {
    List<Waypoint> waypoints = (jsonMap['waypoints']['data'] as Iterable)
        .map((e) => Waypoint.fromMap(e))
        .toList();

    List<Category> categories = (jsonMap['categorias'] as Iterable)
        .map((c) => Category.fromString(c))
        .toList();

    var path = new MapPath(
        id: jsonMap['id'] as int,
        waypoints: waypoints,
        titulo: jsonMap['titulo'] as String,
        descricao: jsonMap['descricao'],
        categorias: categories,
        onSelect: onSelect);

    if (jsonMap.containsKey('thumbnail') && jsonMap['thumbnail'] != null) {
      List<int> bytes = base64Decode(jsonMap['thumbnail']);
      path.thumbnail =
          Image.memory(image.encodeJpg(image.Image.fromBytes(120, 120, bytes)))
              .image;
    }

    return path;
  }

  Future<String> toJson() async {
    List<Map<String, dynamic>> waypointJsons = [];
    for (var waypoint in waypoints) {
      Map<String, dynamic> json = await waypoint.toJson();
      waypointJsons.add(json);
    }

    Map<String, dynamic> map = {
      "path": {
        "id": id,
        "titulo": titulo,
        "descricao": descricao,
        "categorias": categorias.map((c) => c.name).toList(),
        "waypoints": waypointJsons
      }
    };

    if (thumbnail != null) {
      Completer<ByteData> thubmnailData = new Completer();
      thumbnail
          .resolve(ImageConfiguration.empty)
          .addListener(ImageStreamListener((image, success) {
        thubmnailData.complete(image.image.toByteData());
      }));

      ByteData data = await thubmnailData.future;
      var list =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      map["path"]["thumbnail"] = base64Encode(list);
    }

    return jsonEncode(map);
  }

  MapPath clone() {
    return MapPath(
        id: this.id,
        categorias: this.categorias,
        descricao: this.descricao,
        onSelect: this.onSelect,
        thumbnail: this.thumbnail,
        titulo: this.titulo,
        waypoints: this
            .waypoints
            .map((e) => Waypoint(
                id: e.id,
                titulo: e.titulo,
                descricao: e.descricao,
                image: e.image,
                pathTo: e.pathTo))
            .toList());
  }
}
