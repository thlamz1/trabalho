import 'package:image/image.dart' as img;
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Waypoint {
  final int id;
  List<LatLng> pathTo;
  String titulo;
  String descricao;
  ImageProvider image;

  Waypoint(
      {this.id,
      this.titulo = '',
      this.descricao = '',
      this.pathTo = const [],
      this.image});

  static Waypoint fromMap(Map<String, dynamic> map) {
    List<LatLng> pathFromMap = [];
    for (String coord in map['pathTo']) {
      var coords = coord.split(";");
      pathFromMap.add(LatLng(double.parse(coords[0]), double.parse(coords[1])));
    }

    var waypoint = Waypoint(
        id: map['id'] as int,
        titulo: map['titulo'] as String,
        descricao: map['descricao'] as String,
        pathTo: pathFromMap);

    if (map.containsKey('thumbnail') && map['thumbnail'] != null) {
      List<int> bytes = base64Decode(map['thumbnail']);
      waypoint.image =
          Image.memory(img.encodeJpg(img.Image.fromBytes(120, 120, bytes)))
              .image;
    }

    return waypoint;
  }

  Future<Map<String, dynamic>> toJson() async {
    Map<String, dynamic> map = {
      "id": id,
      "titulo": titulo,
      "descricao": descricao,
      "pathTo":
          pathTo.map((ltlng) => "${ltlng.latitude};${ltlng.longitude}").toList()
    };

    if (image != null) {
      Completer<ByteData> thubmnailData = new Completer();
      image
          .resolve(ImageConfiguration.empty)
          .addListener(ImageStreamListener((image, success) {
        thubmnailData.complete(image.image.toByteData());
      }));

      ByteData data = await thubmnailData.future;
      var list =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      map["thumbnail"] = base64Encode(list);
    }

    return map;
  }

  double distanceTo(Waypoint other) {
    double lat1 = this.pathTo.last.latitude;
    double lon1 = this.pathTo.last.longitude;

    double lat2 = other.pathTo.last.latitude;
    double lon2 = other.pathTo.last.longitude;

    // https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
    // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = lat2 * pi / 180 - lat1 * pi / 180;
    var dLon = lon2 * pi / 180 - lon1 * pi / 180;
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(lat1 * pi / 180) *
            cos(lat2 * pi / 180) *
            sin(dLon / 2) *
            sin(dLon / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var d = R * c;
    return d;
  }
}
