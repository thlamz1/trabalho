import 'package:flutter/material.dart';
import 'package:walker/models/category_type.dart';

// Categoria de trajeto
class Category {
  final String name;
  final CategoryType type;
  final IconData icon;

  const Category(
      {this.name = '',
      this.type = CategoryType.info,
      this.icon = Icons.category});

  static Category fromString(String name) {
    return Categories.get(name);
  }
}

// Classe com lista de categorias estáticas
class Categories {
  // Mobilidade
  static const walking = Category(
      name: 'A pé',
      type: CategoryType.warning,
      icon: Icons.directions_walk_sharp);
  static const cycling = Category(
      name: 'Ciclismo', type: CategoryType.warning, icon: Icons.pedal_bike);

  // Ambiente
  static const trail = Category(
      name: 'Trilha', type: CategoryType.enviroment, icon: Icons.nature_people);

  static const sights = Category(
      name: 'Vistas', type: CategoryType.enviroment, icon: Icons.burst_mode);

  // Acessibilidade
  static const accessible = Category(
      name: 'Acessível', type: CategoryType.info, icon: Icons.accessible);

  static const values = [walking, cycling, trail, sights, accessible];

  static Map<String, Category> _categoryMap = {
    'A pé': walking,
    'Ciclismo': cycling,
    'Trilha': trail,
    'Vistas': sights,
    'Acessível': accessible,
  };
  static get(String categoryName) {
    return _categoryMap[categoryName];
  }
}
