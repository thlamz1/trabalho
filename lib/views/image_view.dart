import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageView extends StatelessWidget {
  final ImageProvider image;
  final double width;
  final double height;
  ImageView({this.image, this.width = 75, this.height = 75});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: image == null
          ? Icon(Icons.camera_alt)
          : Image(image: image, alignment: Alignment.center, fit: BoxFit.cover),
      width: width,
      height: height,
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(width)),
      clipBehavior: Clip.antiAlias,
    );
  }
}
