import 'dart:io';
import 'package:image/image.dart' as image;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:walker/models/category.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/category_pill.dart';
import 'package:walker/providers/pathProvider.dart';
import 'package:walker/views/image_view.dart';

// Tela de criação de paths
class PathCreation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PathCreationState();
}

class _PathCreationState extends State<PathCreation> {
  MapPath path;
  final _formKey = GlobalKey<FormState>();
  final PathProvider pathProvider = PathProvider.getInstance();

  String mapPathValidationMessage = "";

  @override
  void initState() {
    super.initState();
  }

  void finish() {
    if (path.waypoints.length == 0) {
      this.setState(() {
        mapPathValidationMessage = "Crie pelo menos um waypoint";
      });
    }

    if (_formKey.currentState.validate()) {
      Navigator.pop(context, path);
    }
  }

  @override
  Widget build(BuildContext context) {
    this.path = ModalRoute.of(context).settings.arguments as MapPath;
    return Scaffold(
        appBar: AppBar(
          title: Text('Criação de Trajeto'),
          actions: [IconButton(icon: Icon(Icons.done), onPressed: finish)],
        ),
        body: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.always,
            child: ListView(
              children: [
                Center(
                    child: GestureDetector(
                        onTap: () async {
                          final picker = ImagePicker();
                          final pickedImage = await picker.getImage(
                              source: ImageSource.gallery);

                          var img = image
                              .decodeImage(await pickedImage.readAsBytes());
                          var thumbnail = image.copyResize(img, width: 120);
                          this.setState(() {
                            path.thumbnail =
                                Image.memory(image.encodeJpg(thumbnail)).image;
                          });
                        },
                        child: ImageView(
                          image: path.thumbnail,
                          width: 120,
                          height: 120,
                        ))),
                ListTile(
                  title: TextFormField(
                    initialValue: path.titulo,
                    onChanged: (value) {
                      path.titulo = value;
                    },
                    validator: (value) {
                      if (value?.isEmpty ?? false) {
                        return "Preencha o título do trajeto";
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Titulo'),
                  ),
                  leading: Icon(
                    Icons.title,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                ListTile(
                    title: TextFormField(
                      initialValue: path.descricao,
                      onChanged: (value) {
                        path.descricao = value;
                      },
                      validator: (value) {
                        if (value?.isEmpty ?? false) {
                          return "Digite uma descrição para o trajeto";
                        }
                        return null;
                      },
                      maxLines: null,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(labelText: 'Descrição'),
                    ),
                    leading: Icon(
                      Icons.description_sharp,
                      color: Theme.of(context).accentColor,
                    )),
                ListTile(
                  title: MultiSelectChipField(
                      items: Categories.values
                          .map((e) => MultiSelectItem(e, e.name))
                          .toList(),
                      searchable: true,
                      searchHint: 'Categoria',
                      headerColor: Colors.transparent,
                      title: Text(''),
                      itemBuilder: (item, state) {
                        var contains = path.categorias.contains(item.value);
                        return Container(
                            margin: EdgeInsets.symmetric(vertical: 15),
                            child: GestureDetector(
                                child: AnimatedOpacity(
                                    opacity: contains ? 1 : 0.4,
                                    duration: Duration(milliseconds: 50),
                                    child: CategoryPill(item.value)),
                                onTap: () {
                                  contains
                                      ? path.categorias.remove(item.value)
                                      : path.categorias.add(item.value);
                                  state.didChange(path.categorias);
                                }));
                      }),
                  leading: Icon(
                    Icons.category,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                ListTile(
                    leading: Icon(
                      Icons.location_pin,
                      color: Theme.of(context).accentColor,
                    ),
                    title: GestureDetector(
                        onTap: () async {
                          path = await Navigator.pushNamed(
                                  context, '/create/location', arguments: path)
                              as MapPath;
                          this.setState(() {
                            if (path.waypoints.length == 0) {
                              mapPathValidationMessage =
                                  "Crie pelo menos um waypoint";
                            } else {
                              mapPathValidationMessage = "";
                            }
                            FocusScope.of(context).unfocus();
                          });
                        },
                        child: Container(
                          child: Column(children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Desenhar trajeto',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1),
                                  Icon(Icons.arrow_downward)
                                ]),
                            Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  "${path.waypoints.length} waypoints adicionados.",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ))
                          ]),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Theme.of(context).primaryColor)),
                        )),
                    subtitle: Text(mapPathValidationMessage,
                        style: TextStyle(color: Colors.red))),
                ListTile(
                  leading: Icon(Icons.location_pin),
                  title: Container(
                      height: 500,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Theme.of(context).primaryColor)),
                      child: ListView.separated(
                          itemBuilder: (context, index) {
                            return Card(
                                child: ListTile(
                              leading: GestureDetector(
                                  onTap: () async {
                                    final picker = ImagePicker();
                                    final pickedImage = await picker.getImage(
                                        source: ImageSource.gallery);

                                    var img = image.decodeImage(
                                        await pickedImage.readAsBytes());
                                    var thumbnail =
                                        image.copyResize(img, width: 120);
                                    this.setState(() {
                                      path.waypoints[index].image =
                                          Image.memory(
                                                  image.encodeJpg(thumbnail))
                                              .image;
                                    });
                                  },
                                  child: ImageView(
                                      image: path.waypoints[index].image,
                                      width: 40,
                                      height: 40)),
                              title: TextFormField(
                                  initialValue: path.waypoints[index].titulo,
                                  decoration:
                                      InputDecoration(labelText: 'Titulo'),
                                  validator: (value) {
                                    if (value?.isEmpty ?? false) {
                                      return "Digite um título para o waypoint";
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    path.waypoints[index].titulo = value;
                                  }),
                              subtitle: TextFormField(
                                  initialValue: path.waypoints[index].descricao,
                                  keyboardType: TextInputType.multiline,
                                  decoration:
                                      InputDecoration(labelText: 'Descrição'),
                                  validator: (value) {
                                    if (value?.isEmpty ?? false) {
                                      return "Digite uma descrição para o waypoint";
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    path.waypoints[index].descricao = value;
                                  }),
                            ));
                          },
                          separatorBuilder: (context, index) {
                            return Center(
                                child: RotatedBox(
                              quarterTurns: 1,
                              child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    '...',
                                    style: TextStyle(
                                        fontSize: 30, color: Colors.grey),
                                  )),
                            ));
                          },
                          itemCount: path.waypoints.length)),
                ),
                Center(
                    child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          Theme.of(context).buttonColor)),
                  onPressed: () {
                    finish();
                  },
                  child: Text('Criar'),
                )),
              ],
            )));
  }
}
