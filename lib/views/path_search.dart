import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:multi_select_flutter/chip_field/multi_select_chip_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:walker/models/category.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/category_pill.dart';
import 'package:walker/providers/pathProvider.dart';
import 'package:walker/views/image_view.dart';
import 'package:walker/views/path_info.dart';

class PathSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PathSearchState();
}

class _PathSearchState extends State<PathSearch> {
  PathProvider pathProvider;
  MapPath searchPath;
  @override
  void initState() {
    super.initState();
    pathProvider = PathProvider.getInstance();
    searchPath = new MapPath();
  }

  @override
  Widget build(BuildContext context) {
    var paths = pathProvider.paths.where((path) {
      if (searchPath.titulo != "" &&
          !path.titulo
              .toLowerCase()
              .contains(searchPath.titulo.toLowerCase())) {
        return false;
      }
      if (searchPath.categorias.any((c) => !path.categorias.contains(c))) {
        return false;
      }
      return true;
    }).toList();

    return Scaffold(
        appBar: AppBar(
          title: Text("Pesquisar Path"),
        ),
        body: Observer(
            builder: (context) => SizedBox.expand(
                    child: Flex(direction: Axis.vertical, children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        TextFormField(
                          onChanged: (value) {
                            this.setState(() {
                              searchPath.titulo = value;
                            });
                          },
                          decoration: InputDecoration(
                              labelText: "Titulo", icon: Icon(Icons.search)),
                        ),
                        MultiSelectChipField(
                            items: Categories.values
                                .map((e) => MultiSelectItem(e, e.name))
                                .toList(),
                            searchable: true,
                            searchHint: 'Categoria',
                            headerColor: Colors.transparent,
                            title: Text(''),
                            itemBuilder: (item, state) {
                              var contains =
                                  searchPath.categorias.contains(item.value);
                              return Container(
                                  margin: EdgeInsets.symmetric(vertical: 15),
                                  child: GestureDetector(
                                      child: AnimatedOpacity(
                                          opacity: contains ? 1 : 0.4,
                                          duration: Duration(milliseconds: 50),
                                          child: CategoryPill(item.value)),
                                      onTap: () {
                                        contains
                                            ? searchPath.categorias
                                                .remove(item.value)
                                            : searchPath.categorias
                                                .add(item.value);
                                        state.didChange(searchPath.categorias);
                                        this.setState(() {});
                                      }));
                            }),
                      ],
                    ),
                  ),
                  Expanded(
                      child: ListView.builder(
                          itemCount: paths.length,
                          itemBuilder: (context, index) {
                            return PathInfo(
                              path: paths[index],
                              onSwipe: false,
                            );
                          }))
                ]))));
  }
}
