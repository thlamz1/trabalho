import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/pathProvider.dart';
import 'package:walker/providers/path_to_map.dart';
import 'package:walker/providers/socketProvider.dart';
import 'package:walker/views/path_info.dart';
import 'package:http/http.dart' as http;

// Página home
class Home extends StatelessWidget {
  Home() {
    SocketProvider.connect(pathProvider);

    var location = Location();
    location.hasPermission().then((value) async {
      if (value == PermissionStatus.denied) {
        await location.requestPermission();
      }
    });
  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  final Completer<GoogleMapController> _controller = Completer();

  PathProvider pathProvider = PathProvider.getInstance();

  Set<Marker> _getMarkers(paths, context) {
    Set<Marker> markers = Set<Marker>();

    paths.forEach((element) {
      markers
          .addAll(pathToMarkers(element, context, onTap: (waypoint, context) {
        openModal(context, element);
      }));
    });

    return markers;
  }

  Set<Polyline> _getPolylines(paths, context) {
    Set<Polyline> polylines = Set<Polyline>();

    paths.forEach((element) {
      polylines.addAll(pathToPolylines(element, context));
    });
    return polylines;
  }

  @override
  Widget build(BuildContext context) {
    var paths = pathProvider.paths;
    return Observer(builder: (context) {
      return Scaffold(
        body: GoogleMap(
          mapType: MapType.satellite,
          initialCameraPosition: Home._kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          myLocationEnabled: true,
          markers: _getMarkers(paths, context),
          polylines: _getPolylines(paths, context),
        ),
        floatingActionButton:
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          FloatingActionButton(
            heroTag: "btn1",
            child: Icon(Icons.add),
            onPressed: () async {
              MapPath newPath = new MapPath();
              MapPath path = await Navigator.pushNamed(context, '/create',
                  arguments: newPath) as MapPath;
              if (path != null) {
                var uri =
                    new Uri.https('lit-depths-09955.herokuapp.com', '/paths');
                String json = await path.toJson();
                http.post(uri, body: json, headers: {
                  'Content-Type': 'application/json'
                }).then((response) {
                  if (response.statusCode == 201 ||
                      response.statusCode == 200) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Trajeto criado com sucesso!"),
                    ));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Falha na criação do trajeto"),
                    ));
                  }
                }).catchError((error) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Falha na criação do trajeto"),
                  ));
                });
              }
            },
          ),
          FloatingActionButton(
              heroTag: "btn2",
              child: Icon(Icons.search),
              onPressed: () {
                Navigator.pushNamed(context, "/search");
              })
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }
}

Future<Widget> openModal(BuildContext context, MapPath path) {
  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return PathInfo(
          path: path,
        );
      },
      backgroundColor: Colors.transparent);
}
