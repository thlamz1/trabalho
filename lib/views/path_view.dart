import 'package:confirm_dialog/confirm_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/category_pill.dart';
import 'package:walker/views/image_view.dart';

class PathView extends StatelessWidget {
  MapPath path;

  @override
  Widget build(BuildContext context) {
    this.path = ModalRoute.of(context).settings.arguments as MapPath;
    return Scaffold(
      appBar: AppBar(
        title: Text(path.titulo),
        actions: [
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () async {
              var updatedPath = await Navigator.pushNamed(context, '/create',
                  arguments: path.clone()) as MapPath;
              if (updatedPath != null) {
                var uri = new Uri.https('lit-depths-09955.herokuapp.com',
                    '/paths/${updatedPath.id}');
                var json = await updatedPath.toJson();
                put(uri,
                        body: json,
                        headers: {'Content-Type': 'application/json'})
                    .then((response) {
                  Navigator.pop(context, response);
                });
              }
            },
          ),
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                if (await confirm(context,
                    title: Text("Tem certeza que deseja excluir?"),
                    content: Text(""),
                    textOK: Text("Sim"),
                    textCancel: Text("Não"))) {
                  var uri = new Uri.https(
                      'lit-depths-09955.herokuapp.com', '/paths/${path.id}');
                  delete(uri).then((response) {
                    Navigator.pop(context, response);
                  });
                }
              })
        ],
      ),
      body: ListView(
        children: [
          Center(
              child: ImageView(
            image: path.thumbnail,
            width: 120,
            height: 120,
          )),
          ListTile(
            leading: Icon(Icons.title),
            title: Text(
              path.titulo,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          ListTile(
            leading: Icon(Icons.description_sharp),
            title: Text(
              path.descricao,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          ListTile(
            leading: Icon(Icons.category),
            title: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).primaryColor)),
              padding: EdgeInsets.all(8),
              child: Wrap(
                  direction: Axis.horizontal,
                  runAlignment: WrapAlignment.start,
                  runSpacing: 10,
                  children:
                      path.categorias.map((e) => CategoryPill(e)).toList(),
                  spacing: 4),
            ),
          ),
          ListTile(
            leading: Icon(Icons.location_pin),
            title: Container(
                height: 400,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    border: Border.all(color: Theme.of(context).primaryColor)),
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      return Card(
                          child: ListTile(
                        leading: ImageView(
                            image: path.waypoints[index].image,
                            width: 40,
                            height: 40),
                        title: Text(
                          path.waypoints[index].titulo,
                          style: Theme.of(context).textTheme.headline1,
                        ),
                        subtitle: Text(path.waypoints[index].descricao,
                            style: Theme.of(context).textTheme.subtitle1),
                      ));
                    },
                    separatorBuilder: (context, index) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RotatedBox(
                              quarterTurns: 1,
                              child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    '...',
                                    style: TextStyle(
                                        fontSize: 30, color: Colors.grey),
                                  ))),
                          Text(path.waypoints[index]
                                  .distanceTo(path.waypoints[index + 1])
                                  .toStringAsFixed(2) +
                              "km")
                        ],
                      );
                    },
                    itemCount: path.waypoints.length)),
          )
        ],
      ),
    );
  }
}
