import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:walker/models/path.dart';
import 'package:walker/providers/category_pill.dart';
import 'package:walker/views/image_view.dart';

class PathInfo extends StatelessWidget {
  final MapPath path;
  final bool onSwipe;

  PathInfo({this.path, this.onSwipe = true});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onVerticalDragUpdate: (details) async {
          if (onSwipe) {
            int sensitivity = 3;
            if (details.delta.dy < -sensitivity) {
              await Navigator.pushNamed(context, '/view', arguments: path);
              Navigator.pop(context);
            }
          }
        },
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            child: Card(
                child: Container(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                ListTile(
                    onTap: () =>
                        Navigator.pushNamed(context, '/view', arguments: path),
                    leading:
                        ImageView(image: path.thumbnail, width: 40, height: 40),
                    title: Text(
                      path.titulo,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    subtitle: path.descricao.isNotEmpty
                        ? Text(
                            path.descricao,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.subtitle1,
                          )
                        : null),
                Container(
                    height: 30,
                    child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: path.categorias
                            .map((c) => CategoryPill(c))
                            .toList()))
              ]),
              padding: EdgeInsets.all(12),
            ))));
  }
}
