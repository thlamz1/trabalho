import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:walker/models/path.dart';
import 'package:walker/models/waypoint.dart';
import 'package:walker/providers/path_to_map.dart';

class PathLocation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PathLocationState();
}

enum _MapTool { marker, line, delete }

class _PathLocationState extends State<PathLocation> {
  MapPath path;

  final Completer<GoogleMapController> _controller = Completer();
  final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  Waypoint _selectedWaypoint;
  _MapTool _selectedTool = _MapTool.marker;

  // Linha temporária de criação
  List<LatLng> _tempLine = [];
  Polyline _tempPolyline() {
    return Polyline(
        polylineId: PolylineId('temp'),
        color: Colors.yellow,
        width: 3,
        points: _tempLine);
  }

  @override
  Widget build(BuildContext context) {
    this.path = ModalRoute.of(context).settings.arguments as MapPath;
    this._selectedWaypoint =
        path.waypoints.length > 0 ? path.waypoints.last : null;
    if (_selectedWaypoint != null && _tempLine.length == 0) {
      _tempLine = [_selectedWaypoint.pathTo.last];
    }

    return Scaffold(
      body: Stack(children: [
        GoogleMap(
            mapType: MapType.satellite,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            myLocationEnabled: true,
            markers: pathToMarkers(path, context,
                onTap: (element, _) => {
                      if (_selectedTool == _MapTool.delete)
                        {
                          if (element == path.waypoints.last)
                            {
                              if (path.waypoints.length > 1)
                                {
                                  this._tempLine = [
                                    path.waypoints[path.waypoints.length - 2]
                                        .pathTo.last
                                  ]
                                }
                              else
                                {this._tempLine = []}
                            },
                          path.removeWaypoint(element),
                          this.setState(() {})
                        }
                    }),
            polylines:
                [...pathToPolylines(path, context), _tempPolyline()].toSet(),
            onTap: (location) {
              switch (_selectedTool) {
                case _MapTool.marker:
                  var waypoint = Waypoint(pathTo: [..._tempLine, location]);
                  path.addWaypoint(waypoint);
                  _selectedWaypoint = waypoint;

                  // Resetando linha temporária
                  _tempLine = [location];
                  break;
                case _MapTool.line:
                  // Adicionando nó em linha temporária
                  _tempLine.add(location);
                  break;
                case _MapTool.delete:
                  break;
              }
              this.setState(() {});
            }),
        Positioned(
            top: 5,
            left: 5,
            child: FloatingActionButton(
              onPressed: () {
                Navigator.pop(context, path);
              },
              child: Icon(Icons.check),
              backgroundColor: Theme.of(context).primaryColor,
            ))
      ]),
      floatingActionButton: Container(
          width: 240,
          child: Row(
            children: [
              // Ferramenta de marcador
              ElevatedButton(
                  child: Icon(
                    Icons.location_on_sharp,
                    color: _selectedTool == _MapTool.marker
                        ? Colors.white
                        : Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    this.setState(() {
                      _selectedTool = _MapTool.marker;
                    });
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          _selectedTool == _MapTool.marker
                              ? Theme.of(context).accentColor
                              : Colors.grey[50]),
                      padding: MaterialStateProperty.all(EdgeInsets.all(15)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              bottomLeft: Radius.circular(15)))),
                      side: MaterialStateProperty.all(BorderSide(
                          color: Theme.of(context).accentColor, width: 2)))),
              // Ferramenta de deleção
              ElevatedButton(
                  child: Icon(
                    Icons.delete,
                    color: _selectedTool == _MapTool.delete
                        ? Colors.white
                        : Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    if (path.waypoints.length > 0) {
                      this.setState(() {
                        _selectedTool = _MapTool.delete;
                      });
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Crie pelo menos um marcador"),
                        duration: Duration(seconds: 2),
                      ));
                    }
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          _selectedTool == _MapTool.delete
                              ? Theme.of(context).accentColor
                              : Colors.grey[50]),
                      padding: MaterialStateProperty.all(EdgeInsets.all(15)),
                      side: MaterialStateProperty.all(BorderSide(
                          color: Theme.of(context).accentColor, width: 2)))),
              // Ferramenta de linha
              ElevatedButton(
                  child: Icon(
                    Icons.linear_scale,
                    color: _selectedTool == _MapTool.line
                        ? Colors.white
                        : Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    if (path.waypoints.length > 0) {
                      this.setState(() {
                        _selectedTool = _MapTool.line;
                      });
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Crie pelo menos um marcador"),
                        duration: Duration(seconds: 2),
                      ));
                    }
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          _selectedTool == _MapTool.line
                              ? Theme.of(context).accentColor
                              : Colors.grey[50]),
                      padding: MaterialStateProperty.all(EdgeInsets.all(15)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15),
                              bottomRight: Radius.circular(15)))),
                      side: MaterialStateProperty.all(BorderSide(
                          color: Theme.of(context).accentColor, width: 2))))
            ],
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
