# walker

Projeto de Flutter da disciplina INF1300

# T2
- [x] Navegação entre telas: com pelo menos 4 namedRoutes;

- [x] Uso de ListTile  ou Card, contendo um icone/ imagem, e textos, e com reatividade onClick())

- [x] Uso de widgets Row/Column aninhados, ou Grid 

- [x] Uso de widgets Container alterando o layout de widgets child

- [x] Usar stageful widgets

- [x] Uso das classes Theme  e o TextTheme para customizar o look&feel do seu app 

- [x] Uso do widget GestureDetection ou SwipeDetector para redefinir uma gesture "do seu jeito" 

- [x] Fazer acesso HTTP a alguma free API para buscar alguma formação específica (*) e usar async/await e try-fetch para captar erros
